package com.abzyne.registre.controllers;

import com.abzyne.registre.model.Visite;
import com.abzyne.registre.repositories.UserRepository;
import com.abzyne.registre.repositories.VisiteRepository;
import com.abzyne.registre.service.VisiteService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/visite")
@CrossOrigin("*")
public class VisiteController {

    private final VisiteService visiteService;
    private final VisiteRepository visiteRepository;

    public VisiteController(VisiteService visiteService, UserRepository userRepository, VisiteRepository visiteRepository) {
        this.visiteService = visiteService;
        this.visiteRepository = visiteRepository;
    }

    @PostMapping(value = "/add-visite")
    public Visite addVisite(Visite visite){
        return  visiteService.addVisite(visite);
    }

    @GetMapping(value = "/get-visite")
    public List<Visite> getVisite(){
        return visiteRepository.findAll();
    }

}
