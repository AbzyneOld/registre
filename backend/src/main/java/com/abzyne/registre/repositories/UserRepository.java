package com.abzyne.registre.repositories;

import com.abzyne.registre.model.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<AppUser, Integer> {
    Optional<AppUser>  findByUsername(String username);
}
