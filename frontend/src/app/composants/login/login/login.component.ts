import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from "../../../services/authentication.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username : string;
  password : string
  loginError : boolean = false
  constructor(private auth : AuthenticationService, private router : Router) { }

  ngOnInit(): void {
  }

  login(username:string , password : string){
  this.auth.login(username, password).subscribe(
    data => {
      let token = data.headers.get('Authorization')
      localStorage.setItem("token", token)
      this.router.navigate(["/visite/liste"])
    }, error => {
      this.loginError = true;
    }
  )
  }
}
