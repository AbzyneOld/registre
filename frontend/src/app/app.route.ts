import {Route, RouterModule} from "@angular/router";
import {ModuleWithProviders} from "@angular/core";


export const appRoute : Route[] = [

  { path: "", redirectTo: "visite/list", pathMatch: "full" },
  {
    path: 'visite',
    loadChildren: () => import('./composants/visite/visite.module').then(m => m.VisiteModule)
  },
  {
    path: 'auth',
    loadChildren: () => import('./composants/login/login.module').then(m => m.LoginModule)
  },
  { path: "**", redirectTo: "visite/list", pathMatch: "full" },

]

export const appRouter: ModuleWithProviders = RouterModule.forRoot(appRoute);
