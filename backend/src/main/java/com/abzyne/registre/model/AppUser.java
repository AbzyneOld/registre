package com.abzyne.registre.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppUser
{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer	appUserNum;

	private String	nom;
	private String	prenom;

	private String	username;
	private String	email;
	private Integer	role;
	private String	phone;
	private String	password;

	public AppUser(Integer appUserNum, String nom,String prenom, String username, String email, Integer role){
	    this.appUserNum = appUserNum;
	    this.nom = nom;
	    this.prenom = prenom;
	    this.role = role;
	    this.email = email;
	    this.username = username;
    }


}
