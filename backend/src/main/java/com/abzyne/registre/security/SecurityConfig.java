package com.abzyne.registre.security;

import com.abzyne.registre.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter
{
	@Autowired
	private UserDetailsServiceImpl		userDetailsService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder	bCryptPasswordEncoder;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception
	{
		String encrytedPassword = bCryptPasswordEncoder.encode("passwordmpec");
		System.out.println(encrytedPassword);
		auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
	}


	@Override
	protected void configure(HttpSecurity http) throws Exception
	{
		http
			.csrf()
			.disable()
				.cors().configurationSource(request -> new CorsConfiguration().applyPermitDefaultValues()).and()
			.sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and()
			.authorizeRequests()
				.antMatchers("/", "/api/**").permitAll()

			.antMatchers(HttpMethod.POST,"/login")
			.permitAll()
			.anyRequest()
			.authenticated()
			.and()
			.addFilterBefore(new JWTAuthenticationFilter(authenticationManager(), "/login", userRepository), UsernamePasswordAuthenticationFilter.class);
			//.addFilterBefore(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);
	}
}
