package com.abzyne.registre.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

@Entity
@Data @NoArgsConstructor @AllArgsConstructor
public class Visite {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer visiteNum;
    private String nomVisiteur;
    private String prenomVisiteur;
    @ColumnDefault("now()")
    private LocalDateTime dateVisite;

    private LocalTime debut;
    private LocalTime fin;
    private Integer status;
    private String numeroBadge;

}
