import { Component, OnInit } from '@angular/core';
import {Visite} from "../../../model/visite";
import {HttpClient} from "@angular/common/http";
import {AuthenticationService} from "../../../services/authentication.service";

@Component({
  selector: 'app-visite-list',
  templateUrl: './visite-list.component.html',
  styleUrls: ['./visite-list.component.scss']
})
export class VisiteListComponent implements OnInit {
visites : Visite[] = [];
  user: Object = [];
  constructor(private http: HttpClient, private auth : AuthenticationService) { }

  ngOnInit(): void {
    this.getAllUser()

  }

  getLabelStatutsByCode(code){
    if(code == 1) return "En cours"
    else return "Terminé"
  }

  getAllUser(){
    return this.http.get<Array<Visite>>("http://localhost:8080/api/visite/get-visite").subscribe(data => {
      this.visites = data;
    })
  }

}
