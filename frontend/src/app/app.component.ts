import { Component } from '@angular/core';
import {AuthenticationService} from "./services/authentication.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
constructor(private auth : AuthenticationService, private router : Router){

}
  title = 'frontend';

  logout() {
    this.auth.logout();
    this.router.navigate(["/auth/signin"])
  }

  isAuthenticated(){
    return this.auth.isAuthenticated()
  }
}
