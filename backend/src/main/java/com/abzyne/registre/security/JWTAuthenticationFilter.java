package com.abzyne.registre.security;

import com.abzyne.registre.model.AppUser;
import com.abzyne.registre.repositories.UserRepository;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;

public class JWTAuthenticationFilter extends AbstractAuthenticationProcessingFilter
{
	private AuthenticationManager	authenticationManager;

	@Autowired
	private final UserRepository								userRepository;


	public JWTAuthenticationFilter(AuthenticationManager authenticationManager, String url , UserRepository userRepository)
	{
		super(new AntPathRequestMatcher(url));
		setAuthenticationManager(authenticationManager);

		this.userRepository = userRepository;
	}

	@Override
	public Authentication attemptAuthentication(
		HttpServletRequest request,
		HttpServletResponse response
	)
		throws AuthenticationException
	{

		String username = request.getParameter("username");
		String password = request.getParameter("password");

		return getAuthenticationManager()
			.authenticate(new UsernamePasswordAuthenticationToken(username, password, Collections.emptyList()));
	}

	@Override
	protected void successfulAuthentication(
		HttpServletRequest request,
		HttpServletResponse response,
		FilterChain chain,
		Authentication authResult
	)
		throws IOException,
		ServletException
	{
		User u = (User) authResult.getPrincipal();

		AppUser appUser = this.userRepository.findByUsername(u.getUsername()).get();
		AppUser connectedUser = new AppUser(
			appUser.getAppUserNum(),
			appUser.getNom(),
			appUser.getPrenom(),
			appUser.getUsername(),
			appUser.getEmail(),
			appUser.getRole()
		);

		String JWT = Jwts
			.builder()
			.claim("connectedUser", connectedUser)
			.setSubject(u.getUsername())
			.setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
			.signWith(SignatureAlgorithm.HS512, SecurityConstants.SECRET)
			.compact();

		response.addHeader("Access-Control-Expose-Headers", SecurityConstants.HEADER_STRING);
		response.addHeader(SecurityConstants.HEADER_STRING, JWT);
	}
}
