package com.abzyne.registre.security;

import com.abzyne.registre.model.AppUser;
import com.abzyne.registre.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService
{
	@Autowired
	UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
	{
		Optional<AppUser> user = userRepository.findByUsername(username);
		if (!user.isPresent())
			throw new UsernameNotFoundException(username);

		AppUser u = user.get();
		Collection<GrantedAuthority> authorities = new ArrayList<>();
		return new User(u.getUsername(), u.getPassword(), authorities);
	}
}
