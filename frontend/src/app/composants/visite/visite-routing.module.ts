import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {VisiteListComponent} from "./visite-list/visite-list.component";
import {VisiteFormComponent} from "./visite-form/visite-form.component";
import {AuthGuardService} from "../../services/auth-guard.service";


const routes: Routes = [
  {
    path : "list",
    component: VisiteListComponent,
    canActivate : [AuthGuardService]
  },

  {
    path : "create",
    component: VisiteFormComponent,
    canActivate : [AuthGuardService]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VisiteRoutingModule { }
