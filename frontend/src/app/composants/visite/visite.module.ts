import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VisiteRoutingModule } from './visite-routing.module';
import { VisiteListComponent } from './visite-list/visite-list.component';
import { VisiteFormComponent } from './visite-form/visite-form.component';
import {TableModule} from "primeng";


@NgModule({
  declarations: [VisiteListComponent, VisiteFormComponent],
  exports: [
    VisiteListComponent
  ],
  imports: [
    CommonModule,
    VisiteRoutingModule,
    TableModule
  ]
})
export class VisiteModule { }
