import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {map} from "rxjs/operators";
import {Router} from "@angular/router";
import {JwtHelperService} from "@auth0/angular-jwt";
import {AppUser} from "../model/app-user";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http : HttpClient, private router : Router) { }

  login(username: string, password: string):Observable<any> {
    let self = this;

    let body = `?username=${encodeURIComponent(username)}&password=${encodeURIComponent(password)}`;
    return this.http.post("http://localhost:8080/login" + body, {}, {observe: 'response'})
  }

  logout(){
    localStorage.clear()
  }

   isAuthenticated(): boolean{
    let helper : JwtHelperService = new JwtHelperService();
    let token = localStorage.getItem("token")
    if(token){
      let isExpired = helper.isTokenExpired(token)
      return !isExpired
    }
    return false
  }


   connectedUser(){
    let helper : JwtHelperService = new JwtHelperService();
    let token = localStorage.getItem("token")
    let decode = helper.decodeToken(token)
    return decode.connectedUser
  }

}
