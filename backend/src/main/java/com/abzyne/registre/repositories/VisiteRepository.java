package com.abzyne.registre.repositories;

import com.abzyne.registre.model.Visite;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VisiteRepository extends JpaRepository<Visite , Integer> {
}
