export class Visite {
  visiteNum : number;
  prenomVisiteur : string;
  nomVisiteur : string;
  dateVisite : Date;
  debut:String;
  fin : string
  status : number
  numeroBadge : String;

  constructor(){
    this.visiteNum = null;
    this.dateVisite = new Date();
    this.nomVisiteur = null;
    this.prenomVisiteur = null;
    this.status = null;
    this.debut = null;
    this.fin = null;
    this.numeroBadge = null;
  }
}
