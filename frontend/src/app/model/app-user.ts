export class AppUser {
  appUserNum : number
  nom : string;
  prenom : string;
  username : string;
  email : string;
  role: string;
  phone : string;
  password:string;

  constructor(){
    this.appUserNum = null;
    this.nom = null;
    this.prenom = null
    this.username = null
    this.email = null;
    this.role = null;
    this.phone = null
    this.password = null
  }
}
