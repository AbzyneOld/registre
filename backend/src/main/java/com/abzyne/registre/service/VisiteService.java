package com.abzyne.registre.service;

import com.abzyne.registre.model.Visite;
import com.abzyne.registre.repositories.VisiteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VisiteService {

    private final VisiteRepository visiteRepository;

    @Autowired
    public VisiteService(VisiteRepository visiteRepository) {
        this.visiteRepository = visiteRepository;
    }

    public Visite addVisite(Visite visite){
       return this.visiteRepository.save(visite);
    }
}
